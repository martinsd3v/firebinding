package marcelo.firebinding;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;

import com.google.firebase.database.*;

import marcelo.firebinding.databinding.MainBinding;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference referece = database.getReference("usuario");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainBinding dataBind = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);

        //Criando Objeto
        String Nome = "Marcelo Martins";
        String ImageUrl = "https://intellectuallearning.files.wordpress.com/2012/12/nave.jpg";
        final Usuario usuario = new Usuario(Nome, ImageUrl);
        dataBind.setUsuario(usuario);
        referece.setValue(usuario);

        //Adaptador para capiturar alteração de nome do usuario
        TextWatcherAdapter watcherName = new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                usuario.setNome(s.toString());
                referece.setValue(usuario);
            }
        };
        dataBind.EntradaNome.addTextChangedListener(watcherName);

        //Adaptador para capiturar alteração de nome do usuario
        TextWatcherAdapter watcherImage = new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                usuario.setImageUrl(s.toString());
                referece.setValue(usuario);
            }
        };
        dataBind.EntradaImage.addTextChangedListener(watcherImage);

        //Firebase integração
        referece.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Usuario fireUser = dataSnapshot.getValue(Usuario.class);
                usuario.setNome(fireUser.getNome());
                usuario.setImageUrl(fireUser.getImageUrl());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
